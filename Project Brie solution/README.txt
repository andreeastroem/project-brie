Game project.

Party game with upto 4 players

Choose from 4 playable characters with different abilities that battle it out in an arena.

Project will include:
	Different scenes for menues.
	Different playable characters.
	Controller support for splitscreen multiplayer.
	Simplistic artstyle with vector graphics.