#pragma once

namespace Input
{
	namespace Gamepad
	{
		enum EButton
		{
			A,
			B,
			Y,
			X,
			LEFTSHOULDER,
			RIGHTSHOULDER,
			BACK,
			START,
			LEFTSTICK,
			RIGHTSTICK,
			EBUTTONSIZE
		};
		enum EJoystickAxis
		{
			XAXIS,
			YAXIS,
			ZAXIS,
			RAXIS,
			UAXIS,
			VAXIS,
			POVYAXIS,
			POVXAXIS,
			EJOYSTICKAXISSIZE
		};
		enum EControlIndex
		{
			ONE,
			TWO,
			THREE,
			FOUR,
			CONTROLINDEX
		};
	}
}

enum class ECOMPONENTTYPE
{
	RENDER,
	PHYSICS,
	AUDIO,
	SCRIPT
};

namespace obj
{
	enum ECreation
	{
		UNSUCCESSFUL,
		SUCCESSFUL,
		INCOMPLETE
	};
	enum EType {
		NONE,
		TRIANGLE
	};
}