#pragma once

#include "SFML/Graphics.hpp"
#include <vector>

#include "Object.h"
#include "Renderer.h"
#include "EventListener.h"

using namespace std;

class Scene
{
protected:
	vector<Object*> m_objects;
	bool m_isDone;
	string m_next;
	string m_prev;
	string m_name;

	sf::Color m_backgroundColour;
	sf::RenderWindow* m_window;
	Renderer* m_renderer;
	EventListener* m_eventListener;
public:
	Scene();
	Scene(string name, EventListener* eventListener);
	
	virtual void update(float dt) = 0;
	virtual void cleanup() = 0;

	string getName();
	void setNextScene(string next);
	string getNextScene();
	bool isDone();
	void attachObject(Object* object);
	
	virtual void enter(string prev, sf::RenderWindow* window, EventListener* eventListener) = 0;
	virtual void exit() = 0;
};