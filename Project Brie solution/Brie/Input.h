#pragma once

#include "SFML/Window.hpp"

namespace Input
{
	class Mouse
	{
		friend class EventListener;
	private:
		bool m_currButtons[sf::Mouse::ButtonCount];
		bool m_prevButtons[sf::Mouse::ButtonCount];

		int x, y;
	public:
		Mouse();
		~Mouse();

		void update();
		
		bool getMouseButton(sf::Mouse::Button button);
		bool getMouseButtonHeld(sf::Mouse::Button button);
		sf::Vector2f getMousePosition();

	private:
		void setMouseButton(sf::Mouse::Button button, bool state);
		void setMousePosition(int x, int y);
	};

	class Keyboard
	{
		friend class EventListener;
		bool m_currButtons[sf::Keyboard::KeyCount];
		bool m_prevButtons[sf::Keyboard::KeyCount];

	public:
		Keyboard();
		~Keyboard();

		void update();

		bool getKeyPressed(sf::Keyboard::Key key);
		bool getKeyPressedOnce(sf::Keyboard::Key key);

	private:
		void setKey(sf::Keyboard::Key key, bool state);
	};

	class XController
	{
		friend class EventListener;
	public:
		struct GamepadButton
		{
			Gamepad::EControlIndex controlIndex;
			Gamepad::EButton button;
			bool state;
		};
		struct GamepadAxis
		{
			Gamepad::EControlIndex controlIndex;
			Gamepad::EJoystickAxis axis;
			float state;
		};

	private:
		GamepadButton m_prevButtons[40];
		GamepadButton m_currButtons[40];
		GamepadAxis m_axes[28];

		bool m_connected[4];
		int m_numberOfConnected = 0;
	public:
		XController();
		~XController();

		void Update();

		bool GetButtonPress(Gamepad::EControlIndex control, Gamepad::EButton button);
		bool GetButtonPressOnce(Gamepad::EControlIndex control, Gamepad::EButton button);
		float GetAxisValue(Gamepad::EControlIndex control, Gamepad::EJoystickAxis axis);
		bool GetConnectedStatus(Gamepad::EControlIndex control);

	private:
		void SetCurrentButtonPress(Gamepad::EControlIndex control, Gamepad::EButton button, bool state);
		void SetPreviousButtonPress(Gamepad::EControlIndex control, Gamepad::EButton button, bool state);
		void SetAxisValue(Gamepad::EControlIndex control, Gamepad::EJoystickAxis axis, float value);
		void SetConnectedStatus(Gamepad::EControlIndex control, bool state);
	};
};