#pragma once

#include "Component.h"
#include "SFML/Graphics.hpp"

using namespace sf;

class RenderComponent : public Component, public Drawable, public Transformable
{
protected:
	Drawable* m_data;
public:
	virtual void update(float dt) override;
	virtual void cleanup() override = 0;
protected:
	virtual void draw(RenderTarget& target, RenderStates states) const override;
};