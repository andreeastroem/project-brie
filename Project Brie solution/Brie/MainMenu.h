#pragma once

#include "Scene.h"

using namespace std;

class MainMenuScene : public Scene
{
public:
	MainMenuScene();
	MainMenuScene(string name, EventListener* eventListener);

	virtual void update(float dt) override;
	virtual void cleanup() override;


	virtual void enter(string prev, sf::RenderWindow* window, EventListener* eventListener) override;
	virtual void exit() override;
};