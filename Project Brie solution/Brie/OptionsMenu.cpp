#include "stdafx.h"

#include "OptionsMenu.h"

OptionsMenuScene::OptionsMenuScene(string name, EventListener* eventListener)
{
	m_backgroundColour = sf::Color::Green;
	m_name = name;
	m_isDone = false;
	m_renderer = new Renderer();

	m_eventListener = eventListener;
}

OptionsMenuScene::OptionsMenuScene()
{
	m_backgroundColour = sf::Color::Green;
	m_name = "options";
	m_isDone = false;
}

void OptionsMenuScene::update(float dt)
{
	__super::update(dt);

	if (m_eventListener->keyPressOnce(sf::Keyboard::Q))
	{
		m_isDone = true;
		m_next = "main";
	}
}

void OptionsMenuScene::cleanup()
{
	__super::cleanup();

}

void OptionsMenuScene::enter(string prev, sf::RenderWindow* window, EventListener* eventListener)
{
	__super::enter(prev, window, eventListener);
	
	m_window->clear(m_backgroundColour);
	m_window->display();
}

void OptionsMenuScene::exit()
{
	__super::exit();
	
}
