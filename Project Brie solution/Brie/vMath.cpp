#include "stdafx.h"

#include "vMath.h"

#include <math.h>

float vMath::distance(sf::Vector2f u, sf::Vector2f v)
{
	return sqrtf(powf(u.x - v.x, 2) + powf(u.y - v.y, 2));
}
