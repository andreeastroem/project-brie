#include "stdafx.h"

#include "EventListener.h"
#include "Input.h"

using namespace Input;

EventListener::EventListener()
{

}

EventListener::~EventListener()
{

}

bool EventListener::initialise()
{
	m_mouse = new Input::Mouse();
	m_keyboard = new Input::Keyboard();
	m_controllers = new Input::XController();

	printf("Size of classes, mouse: %i\tkeyboard: %i\tcontroller: %i\n", sizeof(Input::Mouse), sizeof(Input::Keyboard), sizeof(Input::XController));
	printf("Size of mouse: %i\tkeyboard: %i\tcontroller: %i\n", sizeof(*m_mouse), sizeof(*m_keyboard), sizeof(*m_controllers));

	return true;
}

bool EventListener::mousePress(sf::Mouse::Button button)
{
	return m_mouse->getMouseButton(button);
}

bool EventListener::mousePressHeld(sf::Mouse::Button button)
{
	return m_mouse->getMouseButtonHeld(button);
}

sf::Vector2f EventListener::cursorPosition()
{
	return m_mouse->getMousePosition();
}

bool EventListener::keyPress(sf::Keyboard::Key key)
{
	return m_keyboard->getKeyPressed(key);
}

bool EventListener::keyPressOnce(sf::Keyboard::Key key)
{
	return m_keyboard->getKeyPressedOnce(key);
}

bool EventListener::XbuttonPress(Gamepad::EControlIndex control, Gamepad::EButton button)
{
	return m_controllers->GetButtonPress(control, button);
}

bool EventListener::XbuttonPressOnce(Input::Gamepad::EControlIndex control, Input::Gamepad::EButton button)
{
	return m_controllers->GetButtonPressOnce(control, button);
}

float EventListener::XaxisValue(Input::Gamepad::EControlIndex control, Input::Gamepad::EJoystickAxis axis)
{
	return m_controllers->GetAxisValue(control, axis);
}

bool EventListener::XisConnected(Input::Gamepad::EControlIndex control)
{
	return m_controllers->GetConnectedStatus(control);
}

bool EventListener::windowClosed()
{
	return m_windowClosed;
}

bool EventListener::update(sf::RenderWindow& m_window)
{
	m_mouse->update();
	m_keyboard->update();
	m_controllers->Update();

	sf::Event e;
	while (m_window.pollEvent(e))
	{
		switch (e.type)
		{
#pragma region Controller events
		case sf::Event::JoystickMoved:
			break;
		case sf::Event::JoystickButtonPressed:
			m_controllers->SetCurrentButtonPress((Gamepad::EControlIndex)e.joystickButton.joystickId,
				(Gamepad::EButton)e.joystickButton.button, true);
			break;
		case sf::Event::JoystickButtonReleased:
			m_controllers->SetCurrentButtonPress((Gamepad::EControlIndex)e.joystickButton.joystickId,
				(Gamepad::EButton)e.joystickButton.button, false);
			break;

		case sf::Event::JoystickConnected:
			m_controllers->SetConnectedStatus((Gamepad::EControlIndex)e.joystickConnect.joystickId, true);
			break;
		case sf::Event::JoystickDisconnected:
			m_controllers->SetConnectedStatus((Gamepad::EControlIndex)e.joystickConnect.joystickId, false);
			break;
#pragma endregion Controller events

#pragma region Mouse events
		case sf::Event::MouseButtonPressed:
			m_mouse->setMouseButton(e.mouseButton.button, true);
			break;
		case sf::Event::MouseButtonReleased:
			m_mouse->setMouseButton(e.mouseButton.button, false);
			break;
		case sf::Event::MouseWheelMoved:
			m_mouse->setMousePosition(e.mouseButton.x, e.mouseButton.y);
			break;
		case sf::Event::MouseWheelScrolled:
			//WHAT TO DO?
			break;
#pragma endregion Mouse events

#pragma region Keyboard events
		case sf::Event::KeyPressed:
			m_keyboard->setKey(e.key.code, true);
			break;
		case sf::Event::KeyReleased:
			m_keyboard->setKey(e.key.code, false);
			break;
#pragma endregion Keyboard events
		
		case sf::Event::Closed:
			m_windowClosed = true;
			return false;
			break;
		default:
			break;
		}
	}

	return true;
}

void EventListener::cleanup()
{
	if (m_mouse)
	{
		delete m_mouse;
		m_mouse = nullptr;
	}
	if (m_keyboard)
	{
		delete m_keyboard;
		m_keyboard = nullptr;
	}
	if (m_controllers)
	{
		delete m_controllers;
		m_controllers = nullptr;
	}
}
