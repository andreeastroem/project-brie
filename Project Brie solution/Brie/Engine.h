#pragma once

#include "SFML/Graphics.hpp"

class SceneDirector;
class EventListener;
class Engine
{
	sf::RenderWindow m_window;
	EventListener* m_eventListener;
	SceneDirector* m_sceneDirector;

	sf::Clock m_gameClock;

	bool m_paused = false;
public:
	Engine();
	~Engine();

	bool initialise();
	void update();
	void cleanup();

private:
	float deltatime();
};