#pragma once

#include "SFML/System/Vector2.hpp"

class vMath
{
public:
	static float distance(sf::Vector2f u, sf::Vector2f v);
};