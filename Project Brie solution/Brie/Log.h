/************************************************************************/
/* Logging class for output to console and files
/* Use this class for bool checks that happen once. Such as initialisation
/* and creation, not as a runtime check. Then use printf or equivalent
/************************************************************************/

#pragma once

#define LOGNORMAL 7
#define LOGERROR 12
#define LOGSUCCESSFUL 10
#define PROBLEM 14


class Log
{
public:
	enum EOUTPUTTYPE
	{
		NORMAL = LOGNORMAL,
		ERROR = LOGERROR,
		SUCCESSFUL = LOGSUCCESSFUL,
		WARNING = PROBLEM
	};

	static void Message(const char* message, EOUTPUTTYPE colour = NORMAL);
private:

	static bool WriteToFile();			//NOT YET IMPLEMENTED
	static int GetTime();

public:

private:

};