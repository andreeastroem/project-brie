#include "stdafx.h"

#include "SceneDirector.h"


SceneDirector::SceneDirector(sf::RenderWindow& window, EventListener* eventListener)
{
	m_window = &window;
	m_sceneFactory = new SceneFactory();
	m_current = nullptr;
	m_eventListener = eventListener;
}

void SceneDirector::update(float dt)
{
	m_current->update(dt);
	if (m_current->isDone())
	{
		if (!switchScene(m_current->getNextScene()))
		{
			printf("ERROR SWITCHING SCENES : %s : NO SUCH SCENE\n", m_current->getNextScene().c_str());
		}
	}
}

void SceneDirector::cleanup()
{
	if (m_current)
		m_current = nullptr;
	if (m_sceneFactory)
	{
		delete m_sceneFactory;
		m_sceneFactory = nullptr;
	}
	auto it = m_scenes.begin();
	while (it != m_scenes.end())
	{
		it->second->cleanup();
		delete it->second;
		it->second = nullptr;
		it++;
	}
}

void SceneDirector::AddScene(string name)
{
	Scene* scene = m_sceneFactory->createScene(name, m_eventListener);
	if (scene)
	{
		pair<string, Scene*> scenePair = pair<string, Scene*>(name, scene);
		m_scenes.insert(scenePair);

		if (!m_current)
		{
			m_current = scene;
			m_current->enter("none", m_window, m_eventListener);
		}
	}
}

void SceneDirector::deleteScene(string name)
{
	auto it = m_scenes.find(name);
	if (it != m_scenes.end())
	{
		delete it->second;
		it->second = nullptr;
		m_scenes.erase(name);
	}
}

bool SceneDirector::switchScene(string name)
{
	Scene* next = findScene(name);
	if (next)
	{
		m_current->exit();
		next->enter(m_current->getName(), m_window, m_eventListener);
		m_current = next;
		return true;
	}
	return false;
}

Scene* SceneDirector::findScene(string name)
{
	auto it = m_scenes.find(name);

	if (it != m_scenes.end())
		return it->second;
	else
		return nullptr;
}
