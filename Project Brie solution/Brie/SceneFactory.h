#pragma once

#include "Scene.h"
#include "GameObjectFactory.h"

using namespace std;

class SceneFactory
{
	GOFactory factory;
public:
	SceneFactory();

	Scene* createScene(string name, EventListener* eventListener);

private:
	Scene* createMainMenu(EventListener* eventListener);
	Scene* createOptionsMenu(EventListener* eventListener);
};