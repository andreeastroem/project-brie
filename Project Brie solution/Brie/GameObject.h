#pragma once

#include "Object.h"

class GameObject : public Object
{
	std::string m_name;
public:
	GameObject();

	void Initialise(std::string name);

	virtual void update(float dt) override;
	virtual void cleanup() override;

};