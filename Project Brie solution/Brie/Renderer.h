#pragma once

#include "SFML/Graphics.hpp"
#include <list>
#include "RenderComponent.h"

using namespace sf;
using namespace std;
class Renderer
{
	vector<Drawable*> m_drawables;
public:
	Renderer();
	~Renderer();

	void Render(RenderWindow& window);
	void addToQueue(Drawable* drawable);

private:
};