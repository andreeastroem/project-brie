#pragma once

#include "GameObject.h"

using namespace std;
using namespace obj;

class GOFactory
{
public:
	GOFactory();
	~GOFactory();

	GameObject* createObject(string name, sf::Vector2f pos, vector<string> compList, obj::ECreation& result);

private:
	EType interpretString(string s);

	ECreation createComponent(GameObject& go, EType type);

	ECreation addTriangleComponent(GameObject& go);
};