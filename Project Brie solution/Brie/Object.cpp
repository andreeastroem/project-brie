#include "stdafx.h"

#include "Object.h"
Object::Object()
{

}

void Object::update(float dt)
{
	for each (Component* c in m_renderComponents)
	{
		c->update(dt);
	}
	for each (Component* c in m_physicsComponents)
	{
		c->update(dt);
	}
	for each (Component* c in m_audioComponents)
	{
		c->update(dt);
	}
	for each (Component* c in m_scriptComponents)
	{
		c->update(dt);
	}
}

void Object::cleanup()
{
	for each (Component* c in m_renderComponents)
	{
		c->cleanup();
		delete c;
		c = nullptr;
	}
	for each (Component* c in m_physicsComponents)
	{
		c->cleanup();
		delete c;
		c = nullptr;
	}
	for each (Component* c in m_audioComponents)
	{
		c->cleanup();
		delete c;
		c = nullptr;
	}
	for each (Component* c in m_scriptComponents)
	{
		c->cleanup();
		delete c;
		c = nullptr;
	}
}

void Object::attachComponent(Component* c, ECOMPONENTTYPE type)
{
	switch (type)
	{
	case ECOMPONENTTYPE::RENDER:
		m_renderComponents.push_back(static_cast<RenderComponent*>(c));
		break;
	case ECOMPONENTTYPE::PHYSICS:
		m_physicsComponents.push_back(c);
		break;
	case ECOMPONENTTYPE::AUDIO:
		m_audioComponents.push_back(c);
		break;
	case ECOMPONENTTYPE::SCRIPT:
		m_scriptComponents.push_back(c);
		break;
	}
}

list<RenderComponent*> Object::getRenderInfo()
{
	return m_renderComponents;
}

unsigned int Object::nbrOfComponents()
{
	return m_audioComponents.size() +
		m_physicsComponents.size() +
		m_renderComponents.size() +
		m_scriptComponents.size();
}

void Object::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	for each (Component* c in m_renderComponents)
	{
		target.draw(*static_cast<RenderComponent*>(c), states);
	}
}
