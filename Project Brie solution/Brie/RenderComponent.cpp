#include "stdafx.h"

#include "RenderComponent.h"

void RenderComponent::update(float dt)
{

}

void RenderComponent::cleanup()
{
	if (m_data)
	{
		delete m_data;
		m_data = nullptr;
	}
}

void RenderComponent::draw(RenderTarget& target, RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(*m_data, states);
}
