#include "stdafx.h"

#include "Application.h"
#include "Engine.h"


void Application::run()
{
	Engine e;
	if (e.initialise())
	{
		e.update();
	}
	e.cleanup();
}