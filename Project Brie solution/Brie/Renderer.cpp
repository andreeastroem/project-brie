#include "stdafx.h"

#include "Renderer.h"

Renderer::Renderer()
{
	//m_drawables.resize(1);
}

Renderer::~Renderer()
{

}

void Renderer::Render(RenderWindow& window)
{
	//sort();

	for each (Drawable* drawable in m_drawables)
	{
		window.draw(*drawable);
	}

	window.display();
}


void Renderer::addToQueue(sf::Drawable* drawable)
{
	m_drawables.push_back(drawable);
}
