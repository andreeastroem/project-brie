#pragma once

#include "Scene.h"
#include <map>
#include "SceneFactory.h"

using namespace std;

class SceneDirector
{
	map<string, Scene*> m_scenes;
	Scene* m_current;
	SceneFactory* m_sceneFactory;
	sf::RenderWindow* m_window;
	EventListener* m_eventListener;
public:
	SceneDirector(sf::RenderWindow& window, EventListener* eventListener);

	void update(float dt);
	void cleanup();

	void AddScene(string name);
	void deleteScene(string name);
	bool switchScene(string name);

private:
	Scene* findScene(string name);
};