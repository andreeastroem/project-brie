#include "stdafx.h"

#include "Scene.h"
#include "RenderComponent.h"

using namespace std;

Scene::Scene(string name, EventListener* eventListener)
{
	m_name = name;
	m_isDone = false;
	m_next = "";
	m_prev = "";

	m_renderer = new Renderer();
	m_eventListener = eventListener;
}

Scene::Scene()
{
	m_name = "none";
}

void Scene::update(float dt)
{
	m_window->clear(m_backgroundColour);
	for each (Object* o in m_objects)
	{
		o->update(dt);
		m_renderer->addToQueue(o);
	}
	m_renderer->Render(*m_window);
}

void Scene::cleanup()
{
	for each (Object* o in m_objects)
	{
		o->cleanup();
		delete o;
	}
	m_objects.clear();

	if (m_window)
	{
		m_window = nullptr;
	}

	if (m_renderer)
	{
		delete m_renderer;
		m_renderer = nullptr;
	}
	if (m_eventListener)
	{
		m_eventListener = nullptr;
	}
}

std::string Scene::getName()
{
	return m_name;
}

void Scene::setNextScene(string next)
{
	m_next = next;
}

string Scene::getNextScene()
{
	return m_next;
}

bool Scene::isDone()
{
	return m_isDone;
}

void Scene::attachObject(Object* object)
{
	m_objects.push_back(object);
}

void Scene::enter(string prev, sf::RenderWindow* window, EventListener* eventListener)
{
	m_prev = prev;
	m_window = window;
	m_isDone = false;
	m_renderer = new Renderer();
	m_eventListener = eventListener;
}

void Scene::exit()
{
	//cleanup();
}
