#pragma once

#include "RenderComponent.h"

class TriangleComponent : public RenderComponent
{
public:
	TriangleComponent(unsigned int radius);
	~TriangleComponent();



	virtual void update(float dt) override;


	virtual void cleanup() override;

protected:

};