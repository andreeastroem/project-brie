#pragma once

#include "Scene.h"

using namespace std;

class OptionsMenuScene : public Scene
{
public:
	OptionsMenuScene();
	OptionsMenuScene(string name, EventListener* eventListener);

	virtual void update(float dt) override;
	virtual void cleanup() override;


	virtual void enter(string prev, sf::RenderWindow* window, EventListener* eventListener) override;
	virtual void exit() override;

};