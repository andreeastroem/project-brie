#include "stdafx.h"

#include "Input.h"

#include "SFML/Window.hpp"

namespace Input
{
#pragma region XController
	XController::XController()
	{
		GamepadButton button;
		for (int j = 0; j < Gamepad::EControlIndex::CONTROLINDEX; j++)
		{
			button.controlIndex = (Gamepad::EControlIndex)j;
			for (int i = 0; i < Gamepad::EButton::EBUTTONSIZE; i++)
			{
				button.button = (Gamepad::EButton)i;
				button.state = false;
				//m_currButtons =
				SetCurrentButtonPress(button.controlIndex, button.button, button.state);
				SetPreviousButtonPress(button.controlIndex, button.button, button.state);
			}
		}

		GamepadAxis axis;
		for (int j = 0; j < Gamepad::EControlIndex::CONTROLINDEX; j++)
		{
			axis.controlIndex = (Gamepad::EControlIndex)j;
			for (int i = 0; i < Gamepad::EJoystickAxis::EJOYSTICKAXISSIZE; i++)
			{
				axis.axis = (Gamepad::EJoystickAxis)i;
				axis.state = 0.0f;
				SetAxisValue(axis.controlIndex, axis.axis, axis.state);
			}
		}
	}

	XController::~XController()
	{

	}

	void XController::Update()
	{
		for (unsigned int i = 0; i < 40; i++)
		{
			m_prevButtons[i] = m_currButtons[i];
		}
	}

	bool XController::GetButtonPress(Gamepad::EControlIndex control, Gamepad::EButton button)
	{
		int vectorIndex = 10 * control + Gamepad::EButton::EBUTTONSIZE * button;
		return m_currButtons[vectorIndex].state;
	}

	bool XController::GetButtonPressOnce(Gamepad::EControlIndex control, Gamepad::EButton button)
	{
		int vectorIndex = 10 * control + Gamepad::EButton::EBUTTONSIZE * button;
		return (m_currButtons[vectorIndex].state && !m_prevButtons[vectorIndex].state);

	}

	float XController::GetAxisValue(Gamepad::EControlIndex control, Gamepad::EJoystickAxis axis)
	{
		if (fabs(sf::Joystick::getAxisPosition(control, (sf::Joystick::Axis)axis)) < 15)
			return 0.0f;

		return sf::Joystick::getAxisPosition(control, (sf::Joystick::Axis)axis);
	}

	bool XController::GetConnectedStatus(Gamepad::EControlIndex control)
	{
		return m_connected[control];
	}

	void XController::SetCurrentButtonPress(Gamepad::EControlIndex control, Gamepad::EButton button, bool state)
	{
		int vectorIndex = 10 * control + button;
		m_currButtons[vectorIndex].state = state;
	}

	void XController::SetPreviousButtonPress(Gamepad::EControlIndex control, Gamepad::EButton button, bool state)
	{
		int vectorIndex = 10 * control + button;
		m_currButtons[vectorIndex].state = state;
	}

	void XController::SetAxisValue(Gamepad::EControlIndex control, Gamepad::EJoystickAxis axis, float value)
	{
		int vectorIndex = 10 * control + axis;
		m_axes[vectorIndex].state = value;
	}

	void XController::SetConnectedStatus(Gamepad::EControlIndex control, bool state)
	{
		if (state && m_numberOfConnected < 5)
		{
			m_numberOfConnected++;
			m_connected[control] = state;
		}
		else
		{
			m_numberOfConnected--;
			m_connected[control] = state;
		}
		
	}
#pragma endregion

#pragma region Keyboard

	Keyboard::Keyboard()
	{
		for (int i = 0; i < sf::Keyboard::KeyCount; i++)
		{
			m_currButtons[i] = false;
			m_prevButtons[i] = false;
		}
	}

	Keyboard::~Keyboard()
	{

	}

	void Keyboard::update()
	{
		for (int i = 0; i < sf::Keyboard::KeyCount; i++)
		{
			m_prevButtons[i] = m_currButtons[i];
		}
	}

	bool Keyboard::getKeyPressed(sf::Keyboard::Key key)
	{
		return m_currButtons[key];
	}

	bool Keyboard::getKeyPressedOnce(sf::Keyboard::Key key)
	{
		return m_currButtons[key] && !m_prevButtons[key];
	}

	void Keyboard::setKey(sf::Keyboard::Key key, bool state)
	{
		m_currButtons[key] = state;
	}

#pragma endregion

#pragma region Mouse

	Mouse::Mouse()
	{
		for (int i = 0; i < sf::Mouse::ButtonCount; i++)
		{
			m_currButtons[i] = false;
			m_prevButtons[i] = false;
		}
	}

	Mouse::~Mouse()
	{

	}

	void Mouse::update()
	{
		for (int i = 0; i < sf::Mouse::ButtonCount; i++)
		{
			m_prevButtons[i] = m_currButtons[i];
		}
	}

	bool Mouse::getMouseButton(sf::Mouse::Button button)
	{
		return m_currButtons[button];
	}

	bool Mouse::getMouseButtonHeld(sf::Mouse::Button button)
	{
		return m_currButtons[button] && m_prevButtons[button];
	}

	sf::Vector2f Mouse::getMousePosition()
	{
		return sf::Vector2f(x, y);
	}

	void Mouse::setMouseButton(sf::Mouse::Button button, bool state)
	{
		m_currButtons[button] = state;
	}

	void Mouse::setMousePosition(int x, int y)
	{
		this->x = x;
		this->y = y;
	}

#pragma endregion

}