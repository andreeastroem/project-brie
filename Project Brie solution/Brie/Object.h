#pragma once

#include "Component.h"
#include "SFML/Graphics.hpp"
#include <list>
#include "RenderComponent.h"

using namespace std;

class Object : public sf::Drawable, public sf::Transformable
{
	list<RenderComponent*> m_renderComponents;
	list<Component*> m_physicsComponents;
	list<Component*> m_audioComponents;
	list<Component*> m_scriptComponents;
public:
	Object();

	virtual void update(float dt) = 0;
	virtual void cleanup() = 0;
	
	virtual void attachComponent(Component* c, ECOMPONENTTYPE type);

	list<RenderComponent*> getRenderInfo();
	unsigned int nbrOfComponents();
private:

protected:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

};