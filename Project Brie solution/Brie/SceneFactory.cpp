#include "stdafx.h"

#include <algorithm>

#include "SceneFactory.h"

//scenes
#include "MainMenu.h"
#include "OptionsMenu.h"

SceneFactory::SceneFactory()
{
}

Scene* SceneFactory::createScene(string name, EventListener* eventListener)
{
	std::transform(name.begin(), name.end(), name.begin(), ::tolower);
	Scene* scene;
	
	if (name == "main")
	{
		scene = createMainMenu(eventListener);
		return scene;
	}
	else if (name == "options")
	{
		scene = createOptionsMenu(eventListener);
		return scene;
	}

	return false;
}

Scene* SceneFactory::createMainMenu(EventListener* eventListener)
{
	Scene* scene = new MainMenuScene("main", eventListener);

	GameObject* go;
	obj::ECreation result;
	std::vector<std::string> compList = { "triangle" };
	go = factory.createObject("triangle01", sf::Vector2f(50, 50), compList, result);
	scene->attachObject(go);
	go = factory.createObject("triangle02", sf::Vector2f(150, 50), compList, result);
	scene->attachObject(go);
	go = factory.createObject("triangle03", sf::Vector2f(250, 50), compList, result);
	scene->attachObject(go);

	return scene;
}

Scene* SceneFactory::createOptionsMenu(EventListener* eventListener)
{
	Scene* scene = new MainMenuScene("options", eventListener);

	GameObject* go;
	obj::ECreation result;
	std::vector<std::string> compList = { "triangle" };
	go = factory.createObject("triangle01", sf::Vector2f(50, 50), compList, result);
	if (!go)
		printf("vad fasen.");
	scene->attachObject(go);
	go = factory.createObject("triangle02", sf::Vector2f(50, 150), compList, result);
	scene->attachObject(go);
	go = factory.createObject("triangle03", sf::Vector2f(50, 250), compList, result);
	scene->attachObject(go);

	return scene;
}
