#include "stdafx.h"

#include "Engine.h"
#include "EventListener.h"
#include "SceneDirector.h"

Engine::Engine()
{

}

Engine::~Engine()
{
	cleanup();
}

bool Engine::initialise()
{
	//Create window
	m_window.create(sf::VideoMode(800, 640), "Project Brie");

	m_eventListener = new EventListener();
	m_eventListener->initialise();
	
	m_sceneDirector = new SceneDirector(m_window, m_eventListener);
	m_sceneDirector->AddScene("main");
	m_sceneDirector->AddScene("options");

	m_gameClock.restart();

	return true;
}

void Engine::update()
{
	bool run = true;
	while (run)
	{
		
		m_eventListener->update(m_window);

		if (!m_paused)
		{
			m_sceneDirector->update(deltatime());
		}
		
		if (m_eventListener->keyPress(sf::Keyboard::Escape) || m_eventListener->windowClosed())
			run = false;
	}
}

void Engine::cleanup()
{
	if (m_eventListener)
	{
		m_eventListener->cleanup();
		delete m_eventListener;
		m_eventListener = nullptr;
	}
	if (m_sceneDirector)
	{
		m_sceneDirector->cleanup();
		delete m_sceneDirector;
		m_sceneDirector = nullptr;
	}
}

float Engine::deltatime()
{
	return m_gameClock.restart().asSeconds();
}
