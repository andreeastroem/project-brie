#include "stdafx.h"

#include "MainMenu.h"

//testing
#include "GameObject.h"
#include "TriangleComponent.h"

MainMenuScene::MainMenuScene(string name, EventListener* eventListener)
{
	m_name = name;
	m_isDone = false;

	m_renderer = new Renderer();
	m_backgroundColour = sf::Color::Red;

	m_eventListener = eventListener;
}

MainMenuScene::MainMenuScene()
{
	m_backgroundColour = sf::Color::Red;
	m_name = "main";
	m_isDone = false;
}

void MainMenuScene::update(float dt)
{
	__super::update(dt);

	if (m_eventListener->keyPressOnce(sf::Keyboard::Q))
	{
		m_isDone = true;
		m_next = "options";
	}
}

void MainMenuScene::cleanup()
{
	__super::cleanup();
	
}

void MainMenuScene::enter(string prev, sf::RenderWindow* window, EventListener* eventListener)
{
	__super::enter(prev, window, eventListener);

	m_window->clear(m_backgroundColour);
	m_window->display();
}

void MainMenuScene::exit()
{
	__super::exit();
	
}
