#include "stdafx.h"

#include "TriangleComponent.h"

TriangleComponent::TriangleComponent(unsigned int radius)
{
	CircleShape* circle = new CircleShape(radius, 3);
	circle->setFillColor(sf::Color::Yellow);

	m_data = circle;
	
}

TriangleComponent::~TriangleComponent()
{

}

void TriangleComponent::update(float dt)
{
	__super::update(dt);
	
}

void TriangleComponent::cleanup()
{
	__super::cleanup();
	
}
