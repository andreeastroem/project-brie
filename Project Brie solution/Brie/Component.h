#pragma once

class Component
{
public:
	virtual void update(float dt) = 0;
	virtual void cleanup() = 0;
};