#include "stdafx.h"

#include "GameObjectFactory.h"

#include "TriangleComponent.h"


GOFactory::GOFactory()
{

}

GOFactory::~GOFactory()
{

}

GameObject* GOFactory::createObject(string name, sf::Vector2f pos, vector<string> compList, obj::ECreation& result)
{
	GameObject* go = new GameObject();
	go->Initialise(name);
	go->setPosition(pos);

	for each (string s in compList)
	{
		createComponent(*go, interpretString(s));
	}

	if (go->nbrOfComponents() == compList.size())
		result = SUCCESSFUL;
	else if (go->nbrOfComponents() > 0)
		result = INCOMPLETE;
	else
		result = UNSUCCESSFUL;

	return go;
}

obj::EType GOFactory::interpretString(string s)
{
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	if (s == "triangle")
		return TRIANGLE;

	return NONE;
}

obj::ECreation GOFactory::createComponent(GameObject& go,EType type)
{
	switch (type)
	{
	case obj::NONE:
		Log::Message("GOFactory create component but type is NONE", Log::WARNING);
		return UNSUCCESSFUL;
		break;
	case obj::TRIANGLE:
		addTriangleComponent(go);
		break;
	default:
		break;
	}
	return SUCCESSFUL;
}

obj::ECreation GOFactory::addTriangleComponent(GameObject& go)
{
	TriangleComponent* tc = new TriangleComponent(10);
	go.attachComponent(tc, ECOMPONENTTYPE::RENDER);

	return SUCCESSFUL;
}
