#pragma once

#include "SFML/Graphics.hpp"

namespace Input
{
	class Mouse;
	class Keyboard;
	class XController;
}

class EventListener
{
	friend class Engine;
	Input::Mouse* m_mouse;
	Input::Keyboard* m_keyboard;
	Input::XController* m_controllers;
	bool m_windowClosed = false;
public:
	EventListener();
	~EventListener();

	bool initialise();

	bool mousePress(sf::Mouse::Button button);
	bool mousePressHeld(sf::Mouse::Button button);
	sf::Vector2f cursorPosition();

	bool keyPress(sf::Keyboard::Key key);
	bool keyPressOnce(sf::Keyboard::Key key);

	bool XbuttonPress(Input::Gamepad::EControlIndex control, Input::Gamepad::EButton button);
	bool XbuttonPressOnce(Input::Gamepad::EControlIndex control, Input::Gamepad::EButton button);
	float XaxisValue(Input::Gamepad::EControlIndex control, Input::Gamepad::EJoystickAxis axis);
	bool XisConnected(Input::Gamepad::EControlIndex control);

	bool windowClosed();
private:
	bool update(sf::RenderWindow& m_window);
	void cleanup();

};